 - Fix bug where `siac renter -v` wasn't working due to the wrong flag
   being used.
