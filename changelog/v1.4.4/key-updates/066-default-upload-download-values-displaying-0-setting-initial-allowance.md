 - Fix default expected upload/download values displaying 0 when setting an
   initial allowance.
