 - Fixed bug in siafile snapshot code where the `hostKey()` method
   was not used to safely acquire the host pubkey.
