 - Fix threadgroup violation in the watchdog that allowed writing
   to the log file after a shutdown
