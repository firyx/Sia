 - Fixed bug in startup where an error being returned by the renter's blocking
   startup process was being missed
