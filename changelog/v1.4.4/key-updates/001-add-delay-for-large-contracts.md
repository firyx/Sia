 - Add a delay when modifying large contracts on hosts to prevent hosts
   from becoming unresponsive due to massive disk i/o.
