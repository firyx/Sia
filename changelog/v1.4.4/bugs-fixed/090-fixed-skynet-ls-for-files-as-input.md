 - Fixed `siac skynet ls` not working when files were passed as input.
   It is now able to access specific files in the Skynet folder.
